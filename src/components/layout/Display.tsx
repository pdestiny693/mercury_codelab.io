import * as React from 'react';

type DisplayProps = {
    children: React.ReactNode;
};

const Display = (props: DisplayProps) => <main id="display">{props.children}</main>;

export default Display;
