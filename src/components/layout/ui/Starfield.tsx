import * as React from 'react';

type StarfieldProps = {
    children?: React.ReactNode;
};

const NUM_STARS = 5;

// * Generates a pretty starfield for the backdrop
const Starfield = (props: StarfieldProps) => {
    return (
        <div id="starfield">
            {/* This ugly hack below is to generate JSX elements on the spot. */}
            {/* Otherwise it would complain about missing keys, so I had to wrap it in a `Fragment`. */}
            {[...Array(NUM_STARS).keys()].map(function (index) {
                return (
                    <React.Fragment key={index}>
                        <div className="star"></div>
                    </React.Fragment>
                );
            })}
            {props.children}
        </div>
    );
};

export default Starfield;
