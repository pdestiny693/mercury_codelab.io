import * as React from 'react';
import { Link } from 'react-router-dom';
import { IconName } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Navbar = () => {
    const links = [
        // <path>, <icon-name>, <label>
        ['/', 'home', 'Homepage'],
        ['/resume', 'file-alt', 'CV'],
        ['/about', 'info-circle', 'About me'],
        ['/projects', 'cogs', 'Projects'],
    ];

    let makeLinks = function () {
        let links_list = new Array<React.ReactNode>();
        links.forEach((link) => {
            links_list.push(
                <React.Fragment key={links.indexOf(link)}>
                    <Link to={link[0]}>
                        <div className="option">
                            <FontAwesomeIcon icon={link[1] as IconName} />
                            <section>{link[2]}</section>
                        </div>
                    </Link>
                </React.Fragment>
            );
        });

        return links_list;
    };

    return <nav>{makeLinks()}</nav>;
};

export default Navbar;
