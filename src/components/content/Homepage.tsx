import * as React from 'react';

import Banner from '~/components/layout/ui/Banner';
import Starfield from '~/components/layout/ui/Starfield';
import Navbar from '~/components/layout/ui/Navbar';

const Homepage = () => (
    <article id="homepage">
        <Starfield>
            <div id="content">
                <Banner />
                <h1>Marek Małecki</h1>
                <h2>Developer & Artist</h2>

                <Navbar />
            </div>
        </Starfield>
    </article>
);

export default Homepage;
