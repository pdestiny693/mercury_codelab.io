import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';

export function $(what: string) {
    return document.querySelector(what);
}

export function _(what: string) {
    return document.querySelectorAll(what);
}

export function initFontLibrary() {
    library.add(fas);
}
