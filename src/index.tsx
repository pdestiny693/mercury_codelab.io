import * as React from 'react';
import * as ReactDOM from 'react-dom';

import App from './App';
import { $, _, initFontLibrary } from './utils';
import * as serviceWorker from './serviceWorker';

(function () {
    'use strict';

    // Use this to determine loading padding on DOM.
    const LOAD_PADDING = 1000;

    // Resolves when the webpage DOM is done loading
    // and calls the specific callback.
    function onReady(callback: Function) {
        let interval_id = window.setInterval(function () {
            if ($('body') !== undefined) {
                window.clearInterval(interval_id);
                callback.call(onReady);
            }
        }, LOAD_PADDING);
    }

    let init = function () {
        window.addEventListener('DOMContentLoaded', function () {
            let entry_point = $('#root');

            // Initialize fonts and master stylesheet.
            initFontLibrary();
            require('./styles/master.scss');

            // This is for the loader.
            onReady(function () {
                $('body')!.classList.add('loaded');
            });

            // Render the App in strict-mode
            ReactDOM.render(
                <React.StrictMode>
                    <App />
                </React.StrictMode>,
                entry_point
            );

            // No need for a service worker when we will be hosting this online.
            serviceWorker.unregister();
        });
    };

    init();
})();
