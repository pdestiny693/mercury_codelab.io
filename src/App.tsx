import * as React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';

import Wrapper from '~/components/layout/Wrapper';
import * as Content from '~/components/content';

class App extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/">
                        <Wrapper>
                            <Content.Homepage />
                        </Wrapper>
                    </Route>
                    <Route path="/resume">
                        <Wrapper>
                            <Content.Resume />
                        </Wrapper>
                    </Route>
                </Switch>
            </Router>
        );
    }
}

export default App;
